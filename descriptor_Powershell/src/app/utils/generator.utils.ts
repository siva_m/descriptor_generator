var fs = require('fs');
var exec = require('child_process').exec;
var path = require('path');
var config = require('./../../config/config');
var execSync = require('child_process').execSync;
var flag = { v: 0 };


exports.createDescriptorFile = function (data,id) {
    var transactionFile = data["transaction_file"];
    delete data["transaction_file"];
    var objects = data["objects"];
    data["objects"] = [];
    data["objects"].push({ "Object Type": "SQL", "Object Name": `${transactionFile}` });
    objects.forEach(function (object) {
        if (object["Object Type"] !== 'SP') {
            data["objects"].push(object);
        }
    });
    var dirname = config.path.descriptorDirectory;
    if (!fs.existsSync(dirname)) {
        execSync(`mkdir ${dirname} -p`);
    }
    var cmd;
    if (fs.existsSync(config.path.descriptorFolder)) {
        cmd = `cd ${config.path.descriptorFolder};`;
        cmd = cmd + `git pull ${config.git.descriptor_repository} master --allow-unrelated-histories `;
    }
    else {
        cmd = `cd ${config.path.descriptorDirectory};`;
        cmd = cmd + `git clone ${config.git.descriptor_repository};`;
    }
    return new Promise(function (resolve, reject) {
        execCommand(cmd, function () {
            fs.writeFile(config.path.descriptorFolder+'descriptor_'+id, JSON.stringify(data, null, 2), function (err) {
                if (err)
                    throw err;
                console.log('descriptor file created');
                console.log('Pushing to git..........\n');
                var cmd1 = `
                    cd ${config.path.descriptorFolder};
                    git pull ${config.git.descriptor_repository} master
                    git add -A;
                    git commit -m "first commit";
                    git push -u ${config.git.descriptor_repository} master;`;
                execCommand(cmd1, function (flag) { flag.v = 1; });
                if (flag.v = 1) {
                    resolve('Descriptor file created and pushed to git');
                }
                else {
                    reject('Error in pushing descriptor file to git');
                }
            });
        });
    });
};
var execCommand = function (cmd, callback) {
    exec(cmd, function (error, stdout, stderr) {
        if (error)
            throw error;
        console.log(stdout);
        callback(flag);
    });
};
exports.gitClone = function () {
    console.log('Cloning/Pulling from Git ..........\n');
    return new Promise(function (resolve, reject) {
        var cmd;
        if (fs.existsSync(config.path.descriptorFolder)) {
            cmd = `cd ${config.path.descriptorFolder};git pull ${config.git.descriptor_repository} ;`;
        }
        else {
            cmd = `git clone ${config.git.descriptor_repository} ${config.path.descriptorDirectory};`;
        }
        exec(cmd, function (error, stdout, stderr) {
            if (error)
                reject(error);
            console.log(stdout);
            resolve(stdout);
        });
    });
};
exports.readTransactionFileName = function (id) {
    return new Promise(function (resolve, reject) {
        fs.readFile(config.path.descriptorFolder+'descriptor_'+id, function (err, r) {
            if (err)
                reject(err);
            var t = JSON.parse(r)["objects"];
            for (var temp = 0; temp < t.length; temp++) {
                if (t[temp]["Object Type"] == 'SQL') {
                    resolve(t[temp]["Object Name"]);
                }
            }
        });
    });
};
exports.executeSqlCmd = function (data,filename) {
    console.log('Opening Powershell............');
    var cmd = `echo ${config.password.powershell} |sudo -S pwsh;
    sqlcmd -S ${config["db"][data["db_server"]]["ip"]} -U ${config["db"][data["db_server"]]["username"]} -P ${config["db"][data["db_server"]]["password"]} -i ${config.path.transactionFolder}${filename} -b;`;
    exec(cmd, function (error, stdout, stderr) {
        console.log(stdout);
    });
};

exports.gitCloneSource = function(){
   return new Promise(function(resolve,reject){
    var cmd;
    var dirname = config.path.source;
    if (!fs.existsSync(dirname)) {
        console.log(execSync(`mkdir ${dirname} -p`).toString());
    }
    if (fs.existsSync(config.path.buildFolder)) {
        cmd = `cd ${config.path.source};git pull ${config.git.source_repository} master --allow-unrelated-histories;`;
    }
    else {
        cmd = `git clone ${config.git.source_repository} ${config.path.source};`;
    }
    resolve(execSync(cmd).toString());   
   }); 
}