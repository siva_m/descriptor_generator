var fs = require('fs');
var path = require('path');
var config = require('./../../config/config');
var utils = require('./../utils/generator.utils');


(async function () {

    console.log(await utils.gitCloneSource());

    var buildFiles = [];
    var buildFilesId = [];
    var data;

    fs.readdirSync(config.path.buildFolder).filter(function (file) {
        if (path.extname(file).toLowerCase() === '.json') {
            buildFiles.push(file);
        }
    });

    for (var i = 0; i < buildFiles.length; i++) {
        buildFilesId[i] = buildFiles[i].substring(6, 42);
    }

    for (var i = 0; i < buildFiles.length; i++) {
        data = fs.readFileSync(config.path.buildFolder + buildFiles[i]);
        data = JSON.parse(data);
        try {
            console.log(await utils.createDescriptorFile(data,buildFilesId[i]));
            console.log(await utils.gitClone());

            var filename = await utils.readTransactionFileName(buildFilesId[i]);
            utils.executeSqlCmd(data,filename);
        }
        catch(err){
            console.log(err);
        }
    }

}());