'use strict';

module.exports = {
	app: {
		title: 'Skeleton server',
		description: 'Skeleton server',
		url: ''
	},
	port: process.env.NODEJS_PORT || 9000,
	hostname: process.env.NODEJS_IP || 'localhost',
	authorization: 'mysecrettoken',
	git: {
		descriptor_repository:'https://siva:GT-s5360@alpha.ramco.com/repository/siva/sql-descriptor.git',
		source_repository:'https://siva:GT-s5360@alpha.ramco.com/repository/siva/sql-source.git',
	},

	db: {

		bebswarcnv03: {
			username: 'sa',
			password: 'war3sa*',
			ip: '172.16.10.68'
		}

	},

	password:{
		powershell:'wwe2k18beast1'
	},

	path:{
		source:'./../source/',
		buildFolder:'./../source/buildFile/',
		buildFile:'./../source/buildFile/build.json',
		transactionFolder:'./../source/transaction/',
		transactionFile:'./../source/transaction/transaction.sql',
		descriptorDirectory:'./../descriptor/',
		descriptorFolder:'./../descriptor/sql-descriptor/',
		descriptorFile:'./../descriptor/sql-descriptor/descriptor.json'
	}
};
