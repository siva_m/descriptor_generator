var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var fs = require('fs');
var path = require('path');
var config = require('./../../config/config');
var utils = require('./../utils/generator.utils');
(function () {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(yield utils.gitCloneSource());
        var buildFiles = [];
        var buildFilesId = [];
        var data;
        fs.readdirSync(config.path.buildFolder).filter(function (file) {
            if (path.extname(file).toLowerCase() === '.json') {
                buildFiles.push(file);
            }
        });
        for (var i = 0; i < buildFiles.length; i++) {
            buildFilesId[i] = buildFiles[i].substring(6, 42);
        }
        for (var i = 0; i < buildFiles.length; i++) {
            data = fs.readFileSync(config.path.buildFolder + buildFiles[i]);
            data = JSON.parse(data);
            try {
                console.log(yield utils.createDescriptorFile(data, buildFilesId[i]));
                console.log(yield utils.gitClone());
                var filename = yield utils.readTransactionFileName(buildFilesId[i]);
                utils.executeSqlCmd(data, filename);
            }
            catch (err) {
                console.log(err);
            }
        }
    });
}());
//# sourceMappingURL=descriptor.server.controller.js.map