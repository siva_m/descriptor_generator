'use strict';
module.exports = {
    app: {
        title: 'Skeleton server',
        description: 'Skeleton server',
        url: 'http://localhost:8085'
    },
    port: process.env.NODEJS_PORT || 8085,
    hostname: process.env.NODEJS_IP || 'localhost',
    authorization: 'mysecrettoken',
    git: {
        descriptor_repository: 'https://siva:GT-s5360@alpha.ramco.com/repository/siva/sql-descriptor.git',
        source_repository: 'https://siva:GT-s5360@alpha.ramco.com/repository/siva/sql-source.git',
    },
    db: {
        bebswarcnv03: {
            username: 'sa',
            password: 'war3sa*',
            ip: '172.16.10.68'
        }
    },
    password: {
        powershell: 'wwe2k18beast1'
    },
    path: {
        source: './../source/',
        buildFolder: './../source/buildFile/',
        buildFile: './../source/buildFile/Build_9533b579-c89e-4977-8754-4eb299c03550.json',
        transactionFolder: './../source/transaction/',
        transactionFile: './../source/transaction/transaction.sql',
        descriptorDirectory: './../descriptor/',
        descriptorFolder: './../descriptor/sql-descriptor/',
        descriptorFile: './../descriptor/sql-descriptor/descriptor.json'
    }
};
//# sourceMappingURL=development.js.map